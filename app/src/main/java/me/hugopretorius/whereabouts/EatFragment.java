package me.hugopretorius.whereabouts;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


public class EatFragment extends Fragment {


    public EatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_list, container, false);

        // Create a list of places nearby
        final ArrayList<Place> places = new ArrayList<Place>();
        places.add(new Place(R.string.eat_bardellis, R.string.type_restaurant, R.drawable.bardellis_restaurant));
        places.add(new Place(R.string.eat_black_sheep, R.string.type_restaurant, R.drawable.black_sheep_restaurant));
        places.add(new Place(R.string.eat_knead, R.string.type_bakery, R.drawable.knead_bakery_restaurant));
        places.add(new Place(R.string.eat_the_africa, R.string.type_cafe, R.drawable.the_africa_cafe_restaurant));
        places.add(new Place(R.string.eat_van_hunks, R.string.type_restaurant, R.drawable.van_hunks_restaurant));
        places.add(new Place(R.string.eat_chalk_cork, R.string.type_wine_bar, R.drawable.chalk_cork));



        // Create an {@link PlaceAdapter}, whose data source is a list of {@link Place}s. The
        // adapter knows how to create list items for each item in the list.
        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // place_list.xml layout file.
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        // Make the {@link ListView} use the {@link PlaceAdapter} created above, so that the
        // {@link ListView} will display list items for each {@link Place} in the list.
        listView.setAdapter(adapter);



        return rootView;
    }
}
