package me.hugopretorius.whereabouts;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} that displays a list of things to do.
 */
public class DoFragment extends Fragment {

    public DoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_list, container, false);

        // Create a list of places nearby
        final ArrayList<Place> places = new ArrayList<Place>();
        places.add(new Place(R.string.do_camps_bay_beach, R.string.type_shopping, R.drawable.camps_bay_beach));
        places.add(new Place(R.string.do_labie_theatre, R.string.type_sport, R.drawable.labia_theatre));
        places.add(new Place(R.string.do_lions_head_reserve, R.string.type_reserve, R.drawable.lions_head_reserve));
        places.add(new Place(R.string.do_table_mountain_reserve, R.string.type_tourist, R.drawable.table_mountain_reserve));
        places.add(new Place(R.string.do_two_oceans_aquarium, R.string.type_park, R.drawable.two_oceans_aquarium));
        places.add(new Place(R.string.do_waterfront, R.string.type_shopping, R.drawable.waterfront));



        // Create an {@link PlaceAdapter}, whose data source is a list of {@link Place}s. The
        // adapter knows how to create list items for each item in the list.
        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // place_list.xml layout file.
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        // Make the {@link ListView} use the {@link PlaceAdapter} created above, so that the
        // {@link ListView} will display list items for each {@link Place} in the list.
        listView.setAdapter(adapter);




        return rootView;


    }


}
