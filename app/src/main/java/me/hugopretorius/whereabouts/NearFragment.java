package me.hugopretorius.whereabouts;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} that displays a list of POIs nearby.
 */
public class NearFragment extends Fragment {

    public NearFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_list, container, false);

        // Create a list of places nearby
        final ArrayList<Place> places = new ArrayList<Place>();
        places.add(new Place(R.string.near_canal_walk_shopping_centre, R.string.type_shopping, R.drawable.canal_walk_shopping_centre));
        places.add(new Place(R.string.near_mupine_golf_club, R.string.type_sport, R.drawable.mupine_golf_club));
        places.add(new Place(R.string.near_newlands_forest, R.string.type_reserve, R.drawable.newlands_forest));
        places.add(new Place(R.string.near_old_biscuit_mill, R.string.type_tourist, R.drawable.old_biscuit_mill));
        places.add(new Place(R.string.near_rondebosch_common, R.string.type_park, R.drawable.rondebosch_common));
        places.add(new Place(R.string.near_woodbridge_island, R.string.type_tourist, R.drawable.woodbridge_island));



        // Create an {@link PlaceAdapter}, whose data source is a list of {@link Place}s. The
        // adapter knows how to create list items for each item in the list.
        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // place_list.xml layout file.
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        // Make the {@link ListView} use the {@link PlaceAdapter} created above, so that the
        // {@link ListView} will display list items for each {@link Place} in the list.
        listView.setAdapter(adapter);



        return rootView;
    }

}
