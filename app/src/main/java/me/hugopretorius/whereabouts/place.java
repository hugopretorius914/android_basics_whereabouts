package me.hugopretorius.whereabouts;

/**
 * {@link Place} represents a point of interest in the user's current city
 * It contains resource IDs for the address, an optional picture, name, description
 * and optional link to the website
 */

public class Place {

    /** String resource id for the name of the place*/
    private int mPlaceNameId;

    /** String resource id for the name of the place*/
    private int mPlaceTypeId;

    /** Image resource id for the name of the place*/
    private int mImageResourceId;

    /** Constant value that represents no image was provided for this word */
    private static final int NO_IMAGE_PROVIDED = -1;

    /**
     * Create a new place object,
     *
     * @param PlaceNameId is the string resource ID for the name of the Point Of Interest such
     *                    as the name of a restaurant
     * @param PlaceTypeId is the string resource Id for the word in the Miwok language
     * @param ImageResourceId is the resource ID for the audio file associated with this word
     * */

    public Place(int placeNameId, int placeTypeId, int imageResourceId) {
        mPlaceNameId = placeNameId;
        mPlaceTypeId = placeTypeId;
        mImageResourceId = imageResourceId;
    }

    /**
     * Get the string resource ID for the name of the place.
     */
    public int getPlaceNameId() {
        return mPlaceNameId;
    }

    /**
     * Get the string resource ID for the type of the place.
     */
    public int getPlaceTypeId() {
        return mPlaceTypeId;
    }

    /**
     * Return the image resource ID of the place.
     */
    public int getImageResourceId() {
        return mImageResourceId;
    }

    /**
     * Returns whether or not there is an image for this place.
     */
    public boolean hasImage() {
        return mImageResourceId != NO_IMAGE_PROVIDED;
    }

    @Override
    public String toString() {
        return "Place{" +
                "mPlaceNameId=" + mPlaceNameId +
                ", mPlaceTypeId=" + mPlaceTypeId +
                ", mImageResourceId=" + mImageResourceId +
                '}';
    }
}
