package me.hugopretorius.whereabouts;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HotspotsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HotspotsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HotspotsFragment extends Fragment {
    public HotspotsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.place_list, container, false);

        // Create a list of places nearby
        final ArrayList<Place> places = new ArrayList<Place>();
        places.add(new Place(R.string.hot_maestros, R.string.type_restaurant, R.drawable.maestros_on_beach));
        places.add(new Place(R.string.hot_sea_point_promenade, R.string.type_board_walk, R.drawable.sea_point_promenade));
        places.add(new Place(R.string.hot_side_walk_cafe, R.string.type_wine_bar, R.drawable.side_walk_cafe));
        places.add(new Place(R.string.hot_the_book_lounge, R.string.type_tourist, R.drawable.the_book_lounge));
        places.add(new Place(R.string.hot_companys_garden, R.string.type_garden, R.drawable.the_companys_garden));
        places.add(new Place(R.string.hot_watershed, R.string.type_market, R.drawable.watershed));



        // Create an {@link PlaceAdapter}, whose data source is a list of {@link Place}s. The
        // adapter knows how to create list items for each item in the list.
        PlaceAdapter adapter = new PlaceAdapter(getActivity(), places);

        // Find the {@link ListView} object in the view hierarchy of the {@link Activity}.
        // There should be a {@link ListView} with the view ID called list, which is declared in the
        // place_list.xml layout file.
        ListView listView = (ListView) rootView.findViewById(R.id.list);

        // Make the {@link ListView} use the {@link PlaceAdapter} created above, so that the
        // {@link ListView} will display list items for each {@link Place} in the list.
        listView.setAdapter(adapter);

        // Set a click listener to play the audio when the list item is clicked on
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                // Get the {@link Word} object at the given position the user clicked on
                Place place = places.get(position);

                Log.v("NumbersActivity", "Current word: " + place);
            }
        });


        return rootView;
    }
}
